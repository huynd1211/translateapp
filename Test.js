import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Menu, MenuTrigger, MenuOption, MenuOptions, renderers, MenuProvider } from 'react-native-popup-menu';


class Test extends Component {
    constructor(props) {
        super(props);
        this.state = { color: 'blue', textDecorationLine: 'underline', fontSize: 15 };
    }
    // _onClick (){
    //     this.setState({textDecorationLine: 'underline', fontSize:15, color: 'red'})
    // }
    _onClick = ()=>{
        this.setState({ color: 'red'})
    }
    render() {
        const { Popover } = renderers

        return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: '#f5fcff', }}>
                    <TouchableOpacity onPress={this._onClick}>
                        <Text style={this.state}>AAA</Text>
                    </TouchableOpacity>
                </View>
        )
    }
}


const styles = {
    menuOptions: {
        optionsWrapper: {
            backgroundColor: '#fff',
            height: 70,
            width: 100,
            justifyContent: 'center',
            alignItems: 'center',
        },
    },
    popover: {
        backgroundColor: '#fff',
    },

};
const triggerStyles = {
    triggerText: {
        textDecorationLine: 'underline',
        fontSize: 15
    },
}


export default Test;

