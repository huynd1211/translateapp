import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import Translate from './Translate';
import HomeView from './View';
import IcMenu from '../icons/icMenu.png';
import {
    Menu, MenuOption, MenuOptions,
    MenuTrigger, MenuProvider
} from 'react-native-popup-menu';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isView: true,
            checkedView: '\u2713 ',
            checkedTranslate: '    ',
        }
    }
    _View = () => {
        this.setState({
            isView: true,
            checkedView: '\u2713 ',
            checkedTranslate: '    '
        });
    }
    _Translate = () => {
        this.setState({
            isView: false,
            checkedView: '    ',
            checkedTranslate: '\u2713 '
        })
    }
    render() {
        const main = this.state.isView ? (<HomeView />) : (<Translate />)
        const CheckedOption = (props) => (
            <MenuOption
                onSelect={props.onSelect}
                text={props.checked + props.text}
            />
        )
        return (
            <MenuProvider>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Menu >
                            <MenuTrigger style={styles.trigger}>
                                <Image source={IcMenu}
                                    style={{ borderWidth: 1, marginRight: 5 }} />
                            </MenuTrigger>
                            <MenuOptions customStyles={optionsStyles}>
                                <CheckedOption text=' View'
                                    checked={this.state.checkedView}
                                    onSelect={this._View}
                                    customStyles={styles.optionText} />
                                <CheckedOption text=' Translate'
                                    checked={this.state.checkedTranslate}
                                    onSelect={this._Translate}
                                    customStyles={styles.optionText} />
                            </MenuOptions>
                        </Menu>
                    </View>
                    {main}
                </View>
            </MenuProvider>
        )
    }
}
const styles = {
    container: {
        flex: 1
    },
    header: {
        backgroundColor: '#3f9ee9',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

}
const optionsStyles = {
    optionText: {
        // color: 'red',
        marginLeft: 3,

    }
}

export default Home;

