import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

class Test extends Component {

  render() {
    // var key =/youtube/;
    var str = "xxx<yyy><Youtube>AAAAA</Youtube>xxx</yyy>xxxxxx<yyy><youtube>BBBBB</youtube>xxx</yyy>xxxxxx<yyy><youtube>CCCCC</youtube>xxx</yyy>xxx";
    var textStr = str.replace(/(<youtube>)/gi, ' <youtube>').replace(/(<\/youtube>)/gi, '</youtube> ');
    var res = textStr.split(" ")
    console.log(res)
    return (
      <View style={styles.container}>
        {res.map((e, index) =>
          <View key={index}
            style={{}}>
            <Text>{e}</Text>
          </View>
        )}
      </View>
    )
  }
}


const styles = {
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#f5fcff',
  },
  menuOptions: {
    optionsWrapper: {
      backgroundColor: '#1ef584',
      height: 70,
      width: 100,
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
  popover: {
    backgroundColor: '#1ef584',
  },

};
const triggerStyles = {
  triggerText: {
    // textDecorationLine: 'underline',
    fontSize: 15,
  },
}


export default Test;

