import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import icBack from '../icons/icBack.png';
import {
    Menu, MenuTrigger, MenuOption,
    MenuOptions, renderers, MenuProvider
} from 'react-native-popup-menu';


class Translate extends Component {
    constructor(props) {
        super(props);
        this.state = { color: 'blue', };
    }
    
    _onClick = () => {
        console.log('xxx')
        this.setState({ color: 'red' })
    }

    render() {
        var str = "<div><span>Huynd1211: Hi, I'm trying translate this page! Hi, I'm trying translate this page! Hi, I'm trying translate this page! Hi, I'm trying translate this page! Hi, I'm trying translate this page!</span></div>";
        var textStr = str.replace(/(<([^>]+)>)/g, '').replace(/[^a-zA-Z0-9 ]/g, '');
        var res = textStr.split(" ")
        const { Popover } = renderers
        // console.log(res)
        return (
            <MenuProvider>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: '#f5fcff', }}>
                    <View style={{ flexDirection: 'row', flexWrap: "wrap" }}>
                        {res.map((e, index) =>
                            <Menu key={index}
                                renderer={Popover}
                                rendererProps={{ placement: 'auto', anchorStyle: styles.popover }}
                            >
                                <MenuTrigger onPress={this._onClick} >
                                    <Text style={{ color: this.state.color }}>{e} </Text>
                                </MenuTrigger>
                                <MenuOptions customStyles={styles.menuOptions}>
                                    <MenuOption>
                                        <Text>{e}</Text>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                        )}
                    </View>
                </View>
            </MenuProvider>
        )
    }
}


const styles = {
    menuOptions: {
        optionsWrapper: {
            backgroundColor: '#1ef584',
            height: 70,
            width: 100,
            justifyContent: 'center',
            alignItems: 'center',
        },
    },
    popover: {
        backgroundColor: '#1ef584',
    },

};
const triggerStyles = {
    triggerText: {
        // textDecorationLine: 'underline',
        fontSize: 15,
    },
}


export default Translate;

